#!/bin/bash

# Test crawl of a small sample of artifacts.

if [ -z "${DOOP_BENCHMARKS}" ]
then
    echo You must set DOOP_BENCHMARKS for the Java 7 benchmarks to run.
    exit
fi

# These are broken:
# for b in jboss:jboss-cache:1.2.2 it.unimi.dsi:fastutil:7.2.0

# Artifacts that can be compiled as Java 8.
Java_programs=( net.sf.trove4j:trove4j:3.0.3 org.apache.ivy:ivy:2.3.0 com.itextpdf:itextpdf:5.5.11 com.lowagie:itext:4.2.1 com.marklogic:java-client-api:3.0.7 org.apache.ant:ant:1.10.0 joda-time:joda-time:2.9.6 org.jsoup:jsoup:1.10.2 org.perf4j:perf4j:0.9.16 com.a.eye:data-carrier:1.2 org.scijava:minimaven:2.2.0 org.ccil.cowan.tagsoup:tagsoup:1.2.1 com.jamonapi:jamon:2.81 args4j:args4j:2.0.16 )
Java_libs=( org.apache.httpcomponents:httpcore:4.4.5 org.springframework:spring-aop:4.3.8.RELEASE com.google.collections:google-collections:1.0 javax.mail:mail:1.4 com.j256.ormlite:ormlite-core:5.0 org.apache.httpcomponents:httpclient:4.5.3 org.codehaus.jackson:jackson-core-asl:1.9.13 org.scilab.forge:jlatexmath:1.0.6 ru.send-to.rest:UniversalRest-api:0.1.3.1 org.apache.commons:commons-email:1.4 io.vertx:vertx-mail-client:3.4.0 net.sf.jopt-simple:jopt-simple:5.0.3 com.beust:jcommander:1.72 aopalliance:aopalliance:1.0 com.google.code.gson:gson:2.8.1 org.json:json:20151123 org.aspectj:aspectjrt:1.8.10 org.dbflute.utflute:utflute-core:0.7.9-RC1 commons-cli:commons-cli:1.4 com.sun.jersey:jersey-json:1.19.4 com.helger:ph-datetime:8.6.2 )

# Artifacts that don't have sources (need -ignoreSources in the crawler).
# Java_binaries=( jdepend:jdepend:2.9.1 )

# Artifacts that can be compiled as Java 8 and need reflection support.
# Java_refl_artifacts=( org.mockito:mockito-core:2.0.5-beta org.mockito:mockito-all:2.0.2-beta )

# Java 7 artifacts that cannot be compiled as Java 8 (but are binary compatible with it).
Java7_artifacts=( com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:1.4.2 commons-collections:commons-collections:3.2.2 io.cleardb:cdb.dto2extjs.plugin:0.0.1 )

# Artifacts that contain encodings incompatible with UTF-8 (e.g. WINDOWS-1252).
NonUTF8_artifacts=( net.htmlparser.jericho:jericho-html:3.4 commons-httpclient:commons-httpclient:3.1 )

# Android artifacts
# Android_artifacts=( ai.eve:app:1.1.0 com.applozic.communication.message:mobicomkit:4.84 com.applozic.mobiframework:mobicommons:4.84 dog.abcd:antilib:1.1.3 )
Android_artifacts=( com.applozic.communication.message:mobicomkit:4.84 com.applozic.mobiframework:mobicommons:4.84 )
# Android_artifacts=( com.mutualmobile:barricade:0.1.7 ai.eve:app:1.1.0 com.applozic.communication.message:mobicomkit:4.84 com.applozic.mobiframework:mobicommons:4.84 com.applozic.communication.uiwidget:mobicomkitui:4.84 dog.abcd:antilib:1.1.3 )
ANDROID_SDK="${HOME}/doop-benchmarks/Android/stubs/Android/Sdk"

# APK artifacts (no sources, cached analysis projects).
APK_artifacts=( com.android.chrome com.google.android.apps.translate com.instagram.android com.pinterest com.steam.photoeditor jackpal.androidterm )

echo Artifacts to submit:
echo "Normal artifacts (programs)  : ${#Java_programs[@]}"
echo "Normal artifacts (libraries) : ${#Java_libs[@]}"
# echo "Reflection artifacts         : ${#Java_refl_artifacts[@]}"
echo "Java-7 artifacts             : ${#Java7_artifacts[@]}"
echo "Non-UTF-8 artifacts          : ${#NonUTF8_artifacts[@]}"
echo "Android artifacts            : ${#Android_artifacts[@]}"
if [ "${DOOP_GRADLE_PLUGIN_DIR}" != "" ]; then
echo "Android APKs                 : ${#APK_artifacts[@]}"
fi

echo Press ENTER to start test crawl...
read

FACTGEN_OPT="\"fact_gen_cores\":1"
DISCOVER_MAIN="\"discover_main_methods\":\"true\""
DOOP_PARAMS_DEFAULT="-doopParams {${FACTGEN_OPT},${DISCOVER_MAIN}}"
DOOP_PARAMS_DEFAULT_ANDROID="-doopParams {${FACTGEN_OPT}}"
EXTRA_CRAWL_OPTS=""
# EXTRA_CRAWL_OPTS="-dry"

function optPause() {
    PAUSE_TIME=60
    # PAUSE_TIME=0
    echo "Pausing for ${PAUSE_TIME} secs (press ENTER to skip this pause)..."
    read -t ${PAUSE_TIME}
}

for b in "${Java_programs[@]}"
do
    ./crawl.sh ${b} ${EXTRA_CRAWL_OPTS} ${DOOP_PARAMS_DEFAULT} |& tee ${b}.log
    optPause
done

for b in "${Java_libs[@]}"
do
    ./crawl.sh ${b} -doopParams "{\"open_programs\":\"concrete-types\",\"ignore_main_method\":\"true\",${FACTGEN_OPT},${DISCOVER_MAIN}}" ${EXTRA_CRAWL_OPTS} |& tee ${b}.log
    optPause
done

# for b in "${Java_refl_artifacts[@]}"
# do
#     ./crawl.sh ${b} -doopParams '{"reflection_classic":"true"}' ${EXTRA_CRAWL_OPTS} ${DOOP_PARAMS_DEFAULT} |& tee ${b}.log
#     optPause
# done

for b in "${Java7_artifacts[@]}"
do
    ./crawl.sh ${b} --java7 ${DOOP_BENCHMARKS}/JREs/jre1.7/lib ${EXTRA_CRAWL_OPTS} ${DOOP_PARAMS_DEFAULT} |& tee ${b}.log
    optPause
done

for b in "${NonUTF8_artifacts[@]}"
do
    ./crawl.sh ${b} --convertUTF8 ${EXTRA_CRAWL_OPTS} ${DOOP_PARAMS_DEFAULT} |& tee ${b}.log
    optPause
done

for b in "${Android_artifacts[@]}"
do
    ./crawl.sh ${b} --android ${ANDROID_SDK}/platforms/android-25/android.jar ${EXTRA_CRAWL_OPTS} ${DOOP_PARAMS_DEFAULT_ANDROID} |& tee ${b}.log
    optPause
done

# Benchmark Android APKs. We use doop-gradle-plugin to post them.
if [ "${DOOP_GRADLE_PLUGIN_DIR}" == "" ]
then
    echo "You must set DOOP_GRADLE_PLUGIN_DIR to post APKs."
else
    LOGDIR=`pwd`
    pushd ${DOOP_GRADLE_PLUGIN_DIR}
    for state in "${APK_artifacts[@]}"
    do
        echo "Posting: ${state}"
        ./gradlew replayPost -Pargs="${DOOP_GRADLE_PLUGIN_DIR}/states/${state}" |& tee ${LOGDIR}/${state}.log
        optPause
    done
    popd
fi
