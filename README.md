NOTE: This repo is obsolete and has been integrated into the clyze repo (https://bitbucket.org/clyze/clyze/src/devel/crawler/).


This is the clue-crawler project, a utility that downloads artifacts (with their sources and dependencies) from maven central and submits them for analysis to a specified clue server.

# Prerequisites #

1. Clone the [doop-jcplugin](https://bitbucket.org/yanniss/doop-jcplugin) repo.
2. Install the doop-jcplugin locally by running `./gradlew installDist`. This will install the plugin as a standalone app in the directory `/path/to/doop-jcplugin/build/install/doop-jcplugin`. This will be the jcPlugin home required by the clue-crawler (environment variable JCPLUGIN_HOME=/path/to/doop-jcplugin/build/install/doop-jcplugin).

# Usage #

1. Clone the clue-crawler repo.
2. Run `./gradlew installDist`.
3. Run `./path/to/clue-crawler/build/install/clue-crawler/bin/clue-crawler -r [host:port] -u [user] -p [pass] -j [jcPlugin home] -a [artifact]` to analyse the given artifact.

The artifact is specified using maven coordinates, eg. `org.apache.ivy:ivy:2.3.0`.