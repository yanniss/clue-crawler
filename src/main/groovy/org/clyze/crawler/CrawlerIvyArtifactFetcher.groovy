package org.clyze.crawler

import net.lingala.zip4j.core.ZipFile
import net.lingala.zip4j.model.FileHeader
import org.apache.ivy.core.report.ResolveReport
import org.apache.ivy.core.resolve.IvyNode
import org.clyze.fetcher.Artifact
import org.clyze.fetcher.IvyArtifactFetcher
import org.clyze.utils.AndroidDepResolver

class CrawlerIvyArtifactFetcher extends IvyArtifactFetcher {

    CrawlerArtifact fetch(String id, Repo repo, boolean ignoreSources) {
        Artifact art = super.fetch(id, repo, ignoreSources)
        checkIfGradlePlugin(art)
        addSpecialDependencies(id, art)
        return new CrawlerArtifact(art)
    }

    static void checkIfGradlePlugin(Artifact artifact) {
        ZipFile zipFile = new ZipFile(artifact.jar.canonicalPath)
        List<FileHeader> headers = zipFile.getFileHeaders()
        for (FileHeader header : headers) {
            if (header.isDirectory() &&
                header.getFileName().equals("META-INF/gradle-plugins/")) {

                // Add necessary dependencies from local directory.
                String gradleDir = "gradle-3.3-lib"
                println "Using local Gradle dependencies from ${gradleDir}"
                (new File(gradleDir)).eachFile { File f ->
                    if (f.name.endsWith(".jar")) {
                        artifact.dependencies.add(f.canonicalPath)
                    }
                }
            }
        }
    }

    private static void addSpecialDependencies(String id, def artifact) {
        ResolveReport report = artifact.report
        println "Checking ${id} for special dependencies..."
        if (id.contains("okhttp3")) {
            // The okhttp libraries use the ALPN jar, which is part of
            // the boot classpath, not the regular classpath.
            addSpecialDependency("alpn-boot-8.1.7.v20160121.jar", artifact)
        } else if (id.contains("Abendigo-Offset-Scanner")) {
            // Missing -SNAPSHOT dependency (CLUE-156).
            addSpecialDependency("jna-4.3.0.jar", artifact)
        } else if (id.startsWith("org.asciidoctor:asciidoclet:")) {
            // Problematic JDK-dependent JAR (CLUE-157).
            addSpecialDependency("jdk1.8.0_131-tools.jar", artifact)
        } else if (id.startsWith("org.mockito:mockito-all:")) {
            // Missing dependencies.
            addSpecialDependency("ant-1.8.2.jar", artifact)
            addSpecialDependency("hamcrest-all-1.1.jar", artifact)
            addSpecialDependency("junit-4.12.jar", artifact)
            addSpecialDependency("objenesis-2.6.jar", artifact)
        } else if (id.startsWith("org.mockito:mockito-core:2.10")) {
            // Missing dependencies.
            // addSpecialDependency("ant-1.8.2.jar", artifact)
            addSpecialDependency("hamcrest-all-1.1.jar", artifact)
            addSpecialDependency("junit-4.12.jar", artifact)
            // addSpecialDependency("objenesis-2.6.jar", artifact)
        } else if (id.equals("com.sun.jersey:jersey-json:1.19.4")) {
            // This dependency uses the unsupported
            // '${project.version}' syntax.
            addSpecialDependency("jersey-core-1.19.4.jar", artifact)
            // Another missing dependency.
            addSpecialDependency("javax.ws.rs-api-2.1.jar", artifact)
        }

        // Add special Android dependencies.
        IvyNode[] unresolved = report.getUnresolvedDependencies()
        unresolved.each { IvyNode iNode ->
            String artifactId = iNode.id
            if (artifactId.startsWith('com.android.support#')) {
                println "Adding special Android dependency: ${artifactId}"
                addAndroidDep(artifactId, artifact)
            }
        }
    }

    private static void addAndroidDep(String artifactId, def artifact) {
        AndroidDepResolver res = new AndroidDepResolver()
        res.setUseLatestVersion(true)
        try {
            // This assumes ./local.properties.
            res.findSDK(".")
            int hashIdx = artifactId.indexOf("#")
            int semiIdx = artifactId.indexOf(";")
            String group = artifactId.substring(0, hashIdx)
            String name = artifactId.substring(hashIdx + 1, semiIdx)
            String version = artifactId.substring(semiIdx + 1)
            println "Using custom Android resolver for dependency ${group}:${name}:${version}"
            Set<String> deps = res.resolveDependency(".", group, name, version)
            artifact.dependencies.addAll(deps)
            // println "deps = ${deps}"
        } catch (RuntimeException ex) {
            println "Error: ${ex.message}"
        }
    }

    private static void addSpecialDependency(String jarName, def artifact) {
        String jarPath = "custom-deps/${jarName}"
        File jar = new File(jarPath)
        if (jar.exists()) {
            println "Using special dependency: ${jarPath}"
            artifact.dependencies << jar.canonicalPath
        } else {
            println "WARNING: special dependency ${jarPath} does not exist."
        }
    }

    // Returns true on dependencies that crash Doop.
    @Override
    protected boolean badDependency(String depName) {
        boolean b = depName.contains("groovy-all") || depName.contains("groovy-2.")
        if (b) {
            println("bad dependency: ${depName}")
        }
        return b
    }
}
