package org.clyze.crawler

//import groovy.transform.CompileStatic
import org.clyze.utils.JHelper
import com.jfrog.bintray.client.api.handle.*
import com.jfrog.bintray.client.impl.BintrayClient
import com.jfrog.bintray.client.impl.HttpClientConfigurator
import groovy.json.JsonSlurper
import org.apache.http.*
import org.apache.http.util.EntityUtils
import org.apache.http.impl.client.CloseableHttpClient

//@CompileStatic
class BintrayCrawler {

	private static final String USERNAME            = "yanniss"
    private static final String API_KEY             = "c03a03da2044578c26f8706ffc0161b23573b98e"
    private static final String GET_PACKAGES_PREFIX = "repos/bintray/jcenter/packages?start_pos="
    private static final String GET_PACKAGE_PREFIX  = "packages/bintray/jcenter/"    

    private Bintray bintray

    BintrayCrawler connect(String user, String apiKey) {
        bintray = BintrayClient.create(user, apiKey)        
        return this
    }

    BintrayCrawler connectAnon() {
        String url = BintrayClient.BINTRAY_API_URL
        bintray = BintrayClient.create(clientForAnonRequests(url), url, 5, 90000)
        return this
    }

    void crawlPackages(long start) {
        String url = GET_PACKAGES_PREFIX + start
        println url
        HttpResponse response = bintray.get(url, [:])
        int status = response.getStatusLine().getStatusCode()
        if (status == 200) {
            handleCrawlPackagesResponse(response)
        }	
        else {
            HttpEntity entity = response.getEntity()
            String msg = "$url - $status - ${EntityUtils.toString(entity)}".toString()
            throw new RuntimeException(msg)
        }
    }

    String packageMetadata(String id) {
        String url = GET_PACKAGE_PREFIX + id
        println url
        HttpResponse response = bintray.get(url, [:])
        int status = response.getStatusLine().getStatusCode()
        if (status == 200) {
            return EntityUtils.toString(response.getEntity())
        }   
        else {
            HttpEntity entity = response.getEntity()
            String msg = "$url - $status - ${EntityUtils.toString(entity)}".toString()
            throw new RuntimeException(msg)
        }
    }

    void batchRetrievePackageMetadata(File fileIn, File fileOut) {
        int i = 0
        fileOut.withWriter { Writer writer ->
            fileIn.eachLine { String packageId ->
                String jsonString = null
                
                try {
                    jsonString = packageMetadata(packageId)
                }
                catch(all) {
                    println all.getMessage()
                }

                if (jsonString) {
                    Map<String, Object> packageJson = new JsonSlurper().parseText(jsonString) as Map<String, Object>
                    String mavenId = mavenId(packageJson)
                    String version = packageJson.get('latest_version')
                    int followers = getInt(packageJson, 'followers_count', 0)
                    int ratingCount = getInt(packageJson, 'rating_count', 0)
                    int rating = getInt(packageJson, 'rating', 0)
                    String updated = packageJson.get('updated')
                    if (mavenId) {
                        i++
                        writer.write """$packageId|$mavenId:$version|$updated|$followers|$rating|$ratingCount\n"""
                        if (i % 300 == 0) writer.flush()
                    }
                }
            }
        }
    }

    static int getInt(Map<String, Object> map, String key, int defaultValue) {
        Object value = map.get(key)
        if (value != null)
            return value as int
        return defaultValue
    }

    protected static String mavenId(Map<String, Object> packageJson) {
        Collection<String> systemIds = packageJson.get('system_ids') as Collection<String>
        return systemIds ? systemIds[0] : null
    }

    protected static CloseableHttpClient clientForAnonRequests(String url) {
        return new HttpClientConfigurator().
                   hostFromUrl(url).
                   soTimeout(BintrayClient.DEFAULT_TIMEOUT).
                   connectionTimeout(BintrayClient.DEFAULT_TIMEOUT).
                   noRetry().
                   maxTotalConnections(50).
                   defaultMaxConnectionsPerHost(30).
                   getClient()
    }

    protected void handleCrawlPackagesResponse(HttpResponse response) {

        //response.getAllHeaders().each { println it }

        //write the package names to the file
        String responseText = EntityUtils.toString(response.getEntity())
        new JsonSlurper().parseText(responseText).each { Map<String, Object> m ->
            println "package: ${m.get('name')}"
        }

        long total     = response.getFirstHeader("X-RangeLimit-Total")?.getValue()?.toLong()
        long startPos  = response.getFirstHeader("X-RangeLimit-StartPos")?.getValue()?.toLong()
        long endPos    = response.getFirstHeader("X-RangeLimit-EndPos")?.getValue()?.toLong()

        println "$startPos of $endPos / total $total"

        if (endPos > startPos && endPos < total) {
            //proceed to the next call            
            crawlPackages(endPos + 1)
        }
    }

    static void main(String[] args) {

        JHelper.initConsoleLogging("WARN")

        CliBuilder builder = createCliBuilder()
        OptionAccessor cli = builder.parse(args)

        if (!cli || !args) {
            builder.usage()
            return
        }

        if (cli.l) {
            new BintrayCrawler().connect(USERNAME, API_KEY).crawlPackages(cli.l as Long)
            return
        }

        if(cli.p) {
            println new BintrayCrawler().connectAnon().packageMetadata(cli.p as String)
            return
        }

        if (cli.bs) {
            def (input, output) = cli.bs
            new BintrayCrawler().connectAnon().batchRetrievePackageMetadata(new File(input), new File(output))
        }
    }

    private static final CliBuilder createCliBuilder() {
        CliBuilder cli = new CliBuilder(
            usage: "crawl [one of the mutually exclusive options]"
            )
        cli.width = 120

        cli.with {
            l(longOpt: 'list-packages', "Crawls JCenter through its API and prints the names of all packages returned, starting at the given position", 
                                        args:1, argName: "start-pos")
            p(longOpt: 'package',       "Fetch the package metadata from JCenter", 
                                        args:1, argName:"package-id")
            b(longOpt: 'batch',         "Process the input file containing the package names to write their respective metadata in the output file as csv", 
                                        args:2, valueSeparator: ':', argName:'input-output')
        }

        return cli
    }
}
