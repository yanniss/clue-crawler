package org.clyze.crawler

import com.clyze.client.ConsolePrinter
import com.clyze.client.SourceProcessor
import com.clyze.client.web.Helper
import com.clyze.client.web.PostState
import com.clyze.client.web.SnapshotInput
import java.nio.file.Files
import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import groovy.transform.ToString
import net.lingala.zip4j.core.ZipFile
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.util.Zip4jConstants
import org.apache.commons.io.FileUtils
import org.clyze.analysis.AnalysisOption
import org.clyze.doop.core.DoopAnalysisFamily
import org.clyze.fetcher.Artifact
import org.clyze.utils.ContainerUtils
import org.clyze.utils.JHelper

@CompileStatic
@InheritConstructors
@ToString(includeNames=true)
class CrawlerArtifact extends Artifact {

    protected File sourcesDir
    protected File jsonDir
    protected File metadataZip

    // Flag that is set when Android sources are detected.
    boolean foundAndroidSource
    // Doop parameters map.
    Map<String, Object> paramsMap
    // The final analysis options.
    Map options

    CrawlerArtifact(Artifact art) {
        this.id = art.id
        this.report = art.report
        this.jar = art.jar
        this.sourcesJar = art.sourcesJar
        this.dependencies = art.dependencies
    }

    CrawlerArtifact prepare(String jcpHome, String java7Lib, String androidJarLib,
                            boolean convertUTF8, boolean ignoreSources) {
        super.prepare()

        if (!ignoreSources) {
            int lineCount = invokeJcPlugin(jcpHome, java7Lib, androidJarLib, convertUTF8)
            println "Line count: ${lineCount}"
        }

        options = [
            platform : guessPlatform(androidJarLib),
            inputs   : [jar.canonicalPath] as List<String> ,
            libraries: dependencies as List<String>
        ] << paramsMap
        println "Analysis options: ${options}"

        return this
    }

    // Calls the JcPlugin, returns the number of source lines processed.
    int invokeJcPlugin(String jcpHome, String java7Lib, String androidJarLib,
                       boolean convertUTF8) {
        File classesDir = new File(baseOutDir, "classes")
        classesDir.mkdirs()

        sourcesDir = new File(baseOutDir, "source")
        sourcesDir.mkdirs()

        println "Extracting sources to $sourcesDir"
        ZipFile zipFile = new ZipFile(sourcesJar)
        zipFile.extractAll(sourcesDir.canonicalPath)

        jsonDir = new File(baseOutDir, "json")
        jsonDir.mkdirs()

        SourceProcessor sp = new SourceProcessor()
        Map<String, Object> sourceInfo = sp.process(sourcesDir, convertUTF8)
        List<String> sourceFiles = sourceInfo["sourceFiles"] as List<String>
//        int scalaFilesCount = sourceInfo["scalaFilesCount"] as int
//        int groovyFilesCount = sourceInfo["groovyFilesCount"] as int
        int lineCount = sourceInfo["lineCount"] as int
        foundAndroidSource = sourceInfo["foundAndroidSource"]

        println "Invoking jcplugin on ${sourceFiles.size()} source files (${lineCount} lines)"

        /*
        Collection<String> args = ["-processorpath", Main.JCPLUGIN_JAR_PATH] +
                                  ['-Xplugin:TypeInfoPlugin ' + jsonDir.canonicalPath] +
                                  ["-cp", dependencies.join(File.pathSeparator)] +
                                  ["-source", "1.8"] +
                                  ["-target", "1.8"] +
                                  ["-sourcepath", sourcesDir.canonicalPath] +
                                  ["-d", classesDir.canonicalPath] +
                                  [sourceFiles.get(0)]  

        //println args
        String[] compilerArgs = args.toArray(new String[args.size()])
        ToolProvider.getSystemJavaCompiler().run(System.in, System.out, System.err, compilerArgs)        
        */

        def listFileName = "${baseOutDir}/input-source-files.txt"
        def listFile = new File(listFileName)
        sourceFiles.asList().each { listFile << "${it}\n" }

        Collection<String> platform
        if (java7Lib != null) {
            platform = ['-source', '1.7'] +
                       ['-target', '1.7'] +
                       ["-Xbootclasspath:${java7Lib}/rt.jar:${java7Lib}/jce.jar:${java7Lib}/jsse.jar:${java7Lib}/tools.jar" as String]
        } else if (androidJarLib != null) {
            platform = ['-source', '1.7'] +
                       ['-target', '1.7'] +
                       ["-Xbootclasspath:${androidJarLib}" as String]
        } else {
            platform = ['-source', '1.8'] +
                       ['-target', '1.8']
        }

        Collection<String> cmd = ["${jcpHome}/bin/doop-jcplugin" as String] +
                                 ['-encoding', 'UTF-8'] +
                                 platform +
                                 ['-sd', jsonDir.canonicalPath] +
                                 ['-sourcepath', sourcesDir.canonicalPath] +
                                 ['-d', classesDir.canonicalPath] +
                                 ["@${listFileName}" as String]

        Set<String> tmpDirs = new HashSet<>()
        if (dependencies) {
            // The classpath needs JARs; replace AARs with their JAR contents.
            List<String> jarDeps = ContainerUtils.toJars(dependencies as List, false, tmpDirs)
            cmd += ["-cp", jarDeps.join(":")]
        }
        try {
            String cmdAsString = cmd.collect{ it.toString() }.join(" ")
            println "Running: ${cmdAsString}"
            JHelper.runWithOutput(cmd as String[], "JCPLUGIN ")
        }
        catch(all) {
            println "Invocation of jcplugin failed: $all.message"
            println "Proceeding..."
        } finally {
            println "Deleting: ${tmpDirs}"
            tmpDirs.each { FileUtils.deleteQuietly(new File(it)) }
        }
        //Jar/Zip json files
        metadataZip = new File(baseOutDir, "metadata.zip")
        ArrayList<File> jsonFiles = []

        ZipFile metadataZipFile = new ZipFile(metadataZip.canonicalPath)
        ZipParameters params = new ZipParameters()
        params.setCompressionMethod(Zip4jConstants.COMP_DEFLATE)
        params.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL)
        jsonDir.eachFileRecurse { 
            println "Adding $it to jar"
            jsonFiles << it
        }
        metadataZipFile.addFiles(jsonFiles, params)

        return lineCount
    }

    Artifact post(String hostPrefix, String username, String password,
                  boolean cachePost, boolean dry) {
        PostState ps = createPostState("bundle", options)
        if (metadataZip) {
            ps.addFileInput("JCPLUGIN_METADATA", metadataZip.canonicalPath)
        }
        if (sourcesJar) {
            ps.addFileInput("SOURCES_JAR", sourcesJar.canonicalPath)
        }

        if (cachePost) {
            File tmpDir = Files.createTempDirectory("").toFile()
            ps.saveTo(tmpDir)
            println "Saved post state in $tmpDir"
        }

        if (!dry) {
            String clueProject = 'scrap'
            boolean debug = true
            ps.addInput(new SnapshotInput('jvm_platform', false, 'java_8'))
            Helper.postSnapshot(hostPrefix, username, password, clueProject, ps, new ConsolePrinter(debug), debug)
        }

        return this
    }

    /**
     * Create a POST state from an options map.
     */
    private static PostState createPostState(String id, Map<String, Object> options) {
        PostState ps = new PostState(id: id)
        DoopAnalysisFamily family = new DoopAnalysisFamily()
        options.each { k, v ->
            println "Reading option: ${k} -> ${v}"
            if (k == 'inputs') {
                k = 'input-file'
            } else if (k == 'libraries') {
                k = 'library-file'
            } else if (k == 'platforms') {
                k = 'platform-files'
            } else if (k == 'heapdls') {
                k = 'heapdl-file'
            }
            AnalysisOption opt = family.getOptionByName(k)
            if (opt) {
                if (opt.argInputType) {
                    if (opt.multipleValues) {
                        v.findAll(Helper.checkFileEmpty).each {
                            println "Adding entry: ${it}"
                            ps.addFileInput(opt.id, it as String)
                        }
                    } else if (Helper.checkFileEmpty.call(v as String)) {
                        ps.addFileInput(opt.id, v as String)
                    }
                } else {
                    ps.addStringInput(opt.id, v as String)
                }
            } else {
                println "Ignoring option ${k}: ${v}"
            }
        }
        return ps
    }

    private String guessPlatform(String androidJarLib) {
        // Choose Android only if the user said so.
        if (androidJarLib) {
            return "android_25_fulljars"
        } else {
            File java8Mode = new File(baseOutDir, "java8.txt")
            println("Looking for " + java8Mode.getCanonicalPath())
            String javaPlatform = java8Mode.exists()? "java_8" : "java_7"
            if (foundAndroidSource) {
                println("Warning: Android sources were detected but no android.jar has been given, will use platform = " + javaPlatform)
            }
            return javaPlatform
        }
    }
}
