package org.clyze.crawler

import com.clyze.client.web.PostOptions
import groovy.cli.commons.CliBuilder
import groovy.cli.commons.OptionAccessor
import org.apache.commons.cli.DefaultParser
import org.clyze.fetcher.ArtifactFetcher
import org.clyze.utils.FileOps
import org.clyze.utils.JHelper

class FetchArtifact {

    static final String DEFAULT_HOST_PREFIX = 'localhost:8020/clue'

    static void main(String[] args) {

        JHelper.initConsoleLogging("WARN")

        PostOptions options = new PostOptions()
        String artifactId
        String jcpHome      = System.getenv("JCPLUGIN_HOME")
        String java7Lib     = null
        String androidJarLib= null

        CliBuilder builder = createCliBuilder()
        OptionAccessor cli = builder.parse(args)    

        if (!cli || !args) {
            builder.usage()
            return
        }

        if (cli['a']) {
            artifactId = cli['a']
        } else {
            builder.usage()
            return
        }

        parseRemote(cli['r'] as String, options)

        if (cli['user']) {
            options.username = cli['user']
        } else {
            builder.usage()
            return
        }

        if (cli['token']) {
            options.password = cli['token']
        } else {
            builder.usage()
            return
        }

        if (cli['j']) {
            jcpHome = cli['j']
        } else {
            println "Using default jcPlugin home: $jcpHome"
        }

        // Handle custom platforms (Java 7, Android).
        if (cli['l'] && cli['android']) {
            println "Options -l/--java7 and -d/--android are mutually exclusive."
            return
        } else if (cli['l']) {
            java7Lib = cli['l']
            println "Using Java 7 from ${java7Lib}"
        } else if (cli['android']) {
            androidJarLib = cli['android']
            println "Using ${androidJarLib} as android.jar."
        }

        boolean convertUTF8 = cli['convertUTF8'] ? true : false

        ArtifactFetcher.Repo repo = ArtifactFetcher.Repo.JCENTER
        if (cli['m']) {
            println("Fetching $artifactId from maven central")
            repo = ArtifactFetcher.Repo.MAVEN_CENTRAL
        } else {
            println("Fetching $artifactId from jcenter (default)")
        }

        boolean ignoreSources = cli['ignoreSources'] ? true : false
        if (!ignoreSources) {
            FileOps.findDirOrThrow(jcpHome as String, "The value of the jcPlugin home is invalid: $jcpHome")
        }

        CrawlerArtifact art = new CrawlerIvyArtifactFetcher().
                              fetch(artifactId, repo, ignoreSources).
                              prepare(jcpHome, java7Lib, androidJarLib, convertUTF8, ignoreSources)

        art.post(options.getHostPrefix(), options.username, options.password, cli['cache'] as boolean, cli['dry'] as boolean).cleanUp()
    }

    private static parseRemote(String remote, PostOptions options) {
        if (!remote) {
            remote = DEFAULT_HOST_PREFIX
            println "Using default remote ${remote}"
        }

        String[] parts = remote.split(':')
        int len = parts.length
        if (len < 1 || len > 2) {
            throw new RuntimeException("The value of the remote option is invalid: ${remote}")
        }

        options.host = parts[0]
        try {
            String[] parts0 = parts[1].split('/')
            options.port = parts0[0] as int
            options.basePath = parts0[1] ? ('/' + parts[0]) : ''
        } catch (ignored) {
            println "Using default port number: ${options.getHostPrefix()}"
        }
    }

    private static final CliBuilder createCliBuilder() {
        CliBuilder cli = new CliBuilder(
            parser: new DefaultParser (),
            usage: "fetch [options]",
            width: 120
        )

//        Options opts = new Options()
//        opts.addOption(Option.builder('a').longOpt('artifact')
//                .desc('The maven coordinates of the artifact to submit to the clue server for analysis.').hasArgs().argName('[org]:[proj]:[version]').build())
//        opts.addOption(Option.builder('r').longOpt('remote')
//                .desc('The maven coordinates of the artifact to submit to the clue server for analysis.').hasArgs().argName('[org]:[proj]:[version]').build())
//        cli.setOptions(opts)

        cli.with {
            a(longOpt: 'artifact',     "The maven coordinates of the artifact to submit to the clue server for analysis.", args:1, argName:"[org]:[proj]:[version]")
            r(longOpt: 'remote',       "The remote server prefix (default is ${DEFAULT_HOST_PREFIX}).", args:1, argName: "[hostname|ip]:[port][/path]")
            user(longOpt: 'user',      'The username to connect to the remote server.', args:1, argName: "username")
            token(longOpt: 'token',    'The authentication token to use when connecting to the remote server.', args:1, argName: "token")
            j(longOpt: 'jcpHome',      'The path to the jcPlugin home (default is JCPLUGIN_HOME env var).', args:1, argName: "jcpHome")
            l(longOpt: 'java7',        'Compile as Java 7 sources, using the path of Java 7 libs (e.g. rt.jar).', args:1, argName: "jdk7Libs")
            m(longOpt: 'maven',        'Use maven central (jcenter is used by default).')
            convertUTF8(               'Convert source text to UTF-8.')
            dry(longOpt: 'dry',        "Dry run, do not post anything to the server.")
            d(longOpt: 'android',      "Treat artifact as Android code and use android.jar from path.", args:1, argName: "androidJarLib")
            cache(longOpt: 'cachePost',"Cache information before posting to the server, so that the post can be replayed.")
            ignoreSources(             "Ignore sources in the artifact (don't call the jcplugin).")
        }

        return cli
    }
}
