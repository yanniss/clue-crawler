#!/bin/bash

WHITE_LIST_1=demo-white-list-jcenter.txt
WHITE_LIST_2=demo-white-list-test-crawl.txt

echo Sorting white lists...
sort $WHITE_LIST_1 -o $WHITE_LIST_1
sort $WHITE_LIST_2 -o $WHITE_LIST_2
echo Common entries:
comm -12 $WHITE_LIST_2 $WHITE_LIST_1
echo Total entries:
wc -l $WHITE_LIST_1 $WHITE_LIST_2
