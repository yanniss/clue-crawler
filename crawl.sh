#!/bin/bash

if [ -z "${JCPLUGIN_HOME}" ]
then
    echo "Warning: JCPLUGIN_HOME is not set."
fi

CLUE_HOST=$(grep clue_host $HOME/.gradle/gradle.properties | sed 's/clue_host=//')
CLUE_PORT=$(grep clue_port $HOME/.gradle/gradle.properties | sed 's/clue_port=//')
CLUE_USER=$(grep clue_user $HOME/.gradle/gradle.properties | sed 's/clue_user=//')
CLUE_PASS=$(grep clue_password $HOME/.gradle/gradle.properties | sed 's/clue_password=//')

CMD="-u $CLUE_USER -p $CLUE_PASS -r $CLUE_HOST:$CLUE_PORT -a $@"
echo ${CMD}

# Use gradlew to avoid running './gradlew installDist'.
# eval './gradlew run -Pargs="'$CMD'"'

# Use binary for faster multiple crawls.
build/install/clue-crawler/bin/fetch ${CMD}
